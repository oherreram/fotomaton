int pin = 13;
volatile int state = HIGH;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  attachInterrupt(0, blink, RISING);
}

void loop()
{
  digitalWrite(pin, state);
  delay(200);
}

void blink()
{
  Serial.println("0");
  state = !state;
}
