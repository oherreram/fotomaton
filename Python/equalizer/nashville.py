import cv, subprocess


#convert  1.jpg -channel green -level 0%,100%,2 -channel red -level 0%,50%,0.5 2.jpg
#convert  1.jpg -contrast -modulate 140,10,10 -channel green -level 10%,80%,.2 -channel red -level 10%,50%,0.5 2.jpg
#convert 1.jpg -channel B -level 0%,150%,0.6 2.jpg almost there !!
#convert 1.jpg -channel B -level 0%,150%,0.6 -channel G -level 30%,100%,0.6 2.jpg

def effect_nashville(filename):
    ret, error = False, ''
    try:
        res = subprocess.check_output('convert ' + filename + ' -channel B -level 0%,150%,0.6 ' + filename, shell = False, stderr=subprocess.STDOUT)
    except Exception as err:
        error = str(err)
    else:
    	ret = True

    return [ret, error]





print effect_nashville('2.jpg')