# Create your views here.
from django.http import HttpResponse
from pyamf.remoting.gateway.django import DjangoGateway
from photoboothserver.settings import DEBUG
from django.conf import settings

import logging, os, cv, numpy, datetime, pyqrcode, threading, serial, time, subprocess, xmlrpclib, paramiko, subprocess
import time

thumb_prefix = 'thumb_'
photobooth_server_addr = '66.175.213.58'
photobooth_http_server_addr = 'http://' + photobooth_server_addr + ':8000'
username_ex = 'root'
password_ex = 'evalc838'
boothID     = '001' 

def effects(filename, effect):
    ret, error = False, ''
    if effect == 'nashville':
        stri = 'convert ' + filename + ' -channel R -level 0%,100%,1.8 -channel G -level 0%,100%,1.5 ' + filename
    elif effect == 'distortion':
        stri = 'convert ' + filename + ' -virtual-pixel transparent -distort Barrel "0.0 0.0 0.0 1.0   0.0 0.0 0.5 0.5" ' + filename
    else:
        error = 'no recongized effect'
    if error == '':
        try:
            res = subprocess.check_output( stri ,shell = True, stderr=subprocess.STDOUT)
        except Exception as err:
            error = str(err)
        else:
            ret = True
    return [ret, error]
        

def delete_images(fileArray):
    ret, error = False, []
    count = 0
    for img_file in fileArray:
        pos = img_file.rfind('/')
        if pos != -1:
            folder = img_file[ :pos+1]
            fileName = img_file[pos+1:]
            print folder + thumb_prefix + fileName
            try:
                os.remove( folder + thumb_prefix + fileName)
            except Exception as err:
                error.append( 'error ' + str(err) )
            try:
                os.remove( img_file )
            except Exception as err:
                error.append( 'error ' + str(err) )
            count += 1

    if len(fileArray) == count:
        ret = True

    return [ret, error]


def create_image(fileDict, outputFile):
    ret, error = False, []
    if 'header' in fileDict:
        if 'fileList' in fileDict:
            header = fileDict['header']
            fileList = fileDict['fileList']
            if len(fileList) > 0:
                try:
                    header_im = cv.LoadImageM( header )
                except Exception as err:
                    error.append( 'no header file: ' + str(err) )
                else:                    
                    img_mat_array = []
                    img_mat_array.append(header_im)
                    defined_width  = header_im.width
                    defined_height = int( defined_width * 3 / 4 )
                    filelist_height = 0
                    for img_file in fileList:
                        try:
                            im = cv.LoadImageM( img_file )
                        except Exception as err:
                            error.append( 'image ' + img_file + ' error: ' + str(err) )
                        else:                               
                            if im.width != defined_width and im.height != defined_height:
                                im_to_store = cv.CreateMat( defined_height, defined_width, cv.CV_8UC3)
                                cv.Resize(im, im_to_store)
                            else:
                                im_to_store = im
                            img_mat_array.append( im_to_store )
                            filelist_height += 1
                    dst_arr = numpy.asarray( cv.CreateMat( header_im.height + defined_height * filelist_height, defined_width, cv.CV_8UC3) )
                    rows = 0

                    try:
                        for mat in img_mat_array:
                            ar = numpy.asarray(mat)
                            dst_arr[rows:rows+mat.rows, :mat.cols,0] = ar[:,:,0]
                            dst_arr[rows:rows+mat.rows, :mat.cols,1] = ar[:,:,1]
                            dst_arr[rows:rows+mat.rows, :mat.cols,2] = ar[:,:,2]
                            rows += mat.rows
                        dst = cv.fromarray(dst_arr)
                    except Exception as err:
                        error.append( 'color channels error: ' + str(err) )

                    try:
                        print outputFile
                        cv.SaveImage(outputFile, dst)
                    except Exception as err:
                        error.append( 'saving error: ' + str(err) )
                    else:
                        ret = True
            else:
                error.append( E_CREATEIMAGEFILELISTEMPTY[1] )
    else:
        error.append( E_CREATEIMAGENOHEADER[1] )
    return [ret, error]

def save_snapshot(http_request, image, flashPath, flashImageFolder, fileName, thumbWidth, thumbHeight):

    ret, error = False, ''
    
    folder = flashPath + flashImageFolder + '/'
    if not os.path.exists(folder):
        try:
            os.mkdir(folder)
        except Exception as err:
            error = 'error ' + str(err)
    
    if error == '':
        try:
            fp = open( folder + fileName, 'wb+')
            fp.write( image.getvalue() )
            fp.close()
        except Exception as err:
            error = 'error: ' + str(err)
        else:            
            res, error = effects(folder + fileName, 'nashville')
            res, error = effects(folder + fileName, 'distortion')
            if error == '':
                try:
                    im = cv.LoadImageM( folder + fileName )
                except Exception as err:
                    error = 'error ' + str(err)
                else:
                    im_to_store = cv.CreateMat( thumbHeight, thumbWidth, cv.CV_8UC3)
                    cv.Resize(im, im_to_store)
                    outputFile = folder + thumb_prefix + fileName
                    cv.SaveImage( outputFile, im_to_store)
                    ret = True
        	
        	
    return {'ret' : ret, 'fileNameFlashPath' : flashImageFolder + '/' + fileName, 'fileNameFullPath' : folder + fileName, 'thumbFileNameFlashPath' : flashImageFolder + '/' + thumb_prefix + fileName}
    
def sendFile(filePath):
    addr = error = ''
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(photobooth_server_addr, username = username_ex, password = password_ex)
        sftp = ssh.open_sftp()
        pos  = filePath.rfind('/')
        addr_tmp = boothID + '_' + filePath[pos+1:]
        sftp.put(filePath, '/home/fotomaton/photobooth/static_files/' + addr_tmp )
    except Exception as err:
        error = str(err)
    else:
        addr = photobooth_http_server_addr + '/images/' + addr_tmp
        sftp.close()
        ssh.close()
    return [addr, error]


def joinAndPrint(http_request, flashPath, flashImageFolder, static_images_folder, fileArray):
    str_date = datetime.datetime.now().strftime("%d_%m_%y_%H_%M_%S")
    outputFile = 'output_' +  str_date + '.jpg'
    outputQRImage = 'qr_image_' + str_date + '.jpg'
    fileDict = {'header' : flashPath + static_images_folder + '/header.jpg', 'fileList' : fileArray}
    ret, error = create_image(fileDict, settings.STATICFILES_DIRS[0] + '/' + outputFile)
    if ret == True:
        addr, error = sendFile( settings.STATICFILES_DIRS[0] + '/' + outputFile )
        if error == '':
            qr_image = pyqrcode.MakeQRImage( addr )
            qr_image.save( flashPath + flashImageFolder + '/' + outputQRImage )
        ret_del, error = delete_images(fileArray)

    return {'ret' : ret, 'errorex' : error, 'qrimagepath' : flashImageFolder + '/' + outputQRImage}



class CoinDetector(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        self.serial     = serial.Serial(baudrate = 9600)
        self.cd_on      = False;
        self.cd_mutex   = threading.Lock();
        self.coin_count = 0;
    
    def run_check_coin(self):
        self.cd_on = True
        coin = ''
        if self.serial.isOpen() == False:
            self.serial.open()
        prevTime = 0
        while True:
            coin = self.serial.readline().replace('\r','').replace('\n','')
            currentTime = time.time();
            print 'coin inserted'
            if coin == '0': #program software filter with time...
                print 'one credit !!'
                if currentTime - prevTime > 1:
                    self.cd_mutex.acquire()
                    self.coin_count += 1
                    self.cd_mutex.release()
                else:
                    print 'one coin discarted'
            prevTime = currentTime

    def init(self):
        ret, error = False, ''
        try:
            res = subprocess.check_output( 'ls /dev/tty.usbmodem*', shell = True, stderr=subprocess.STDOUT)
        except Exception as err:
            error = str(err)
        else:           
            self.serial.port = res.replace('\n', '')
            try:
                self.thread = threading.Thread(target = self.run_check_coin, name = "CD")
                self.thread.daemon = True
                self.thread.start()
            except Exception as err:
                error = str(err)
            else:
                ret = True

        return [ret, error]

    def check_coin(self, http_request):
        ret, error = 0, ''
        if self.cd_on == False:
            _ret, _error = self.init()
            print 'check {}'.format(_ret)
        else:
            self.cd_mutex.acquire()
            print 'reading coin count'
            ret = self.coin_count
            self.coin_count = 0
            self.cd_mutex.release()

        return {'ret' : ret, 'error_ex' : error}

coin_detector = CoinDetector();

services = {
    'saveSnapshot' : save_snapshot,
    'joinAndPrint' : joinAndPrint
}

services_cd = {
    'checkCoin' : coin_detector.check_coin
}

gw = DjangoGateway(services,    logger = logging, debug = DEBUG)
cd = DjangoGateway(services_cd, logger = logging, debug = DEBUG)

def index(request):
    return HttpResponse("Hello, world. You're at the index.")