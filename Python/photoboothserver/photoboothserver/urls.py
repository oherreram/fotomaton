from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
					  url(r'^$', 'gateway.views.index'),	
					  url(r'^gateway/', 'gateway.views.gw'),
                      url(r'^cd/', 'gateway.views.cd'),
					  url(r'^images/(?P<file_name>.*)', 'images.views.show'),
    # Examples:
    # url(r'^$', 'photoboothserver.views.home', name='home'),
    # url(r'^photoboothserver/', include('photoboothserver.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
