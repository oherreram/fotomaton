# Create your views here.
from django.http import HttpResponse
from django.shortcuts import redirect, render_to_response, get_object_or_404

def show(request, file_name):
	data = {'image_file_name' : file_name}
	return render_to_response('images/show.html', data)